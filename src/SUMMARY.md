# Summary

- [نیکاس](index.md)

- [معرفی](introducing/index.md)
  - [نیکاس چیست ؟](introducing/introducing.md)
  - [نمونه های مشابه](introducing/similar.md)

- [شروع به کار](getting-started/index.md)
  - [نیازمندی ها](getting-started/requirements.md)
  - [نصب](getting-started/install.md)
  - [اجرا](getting-started/run.md)

- [تنظیمات](config/index.md)
  - [سرور](config/server.md)
  - [کلاینت](config/client.md)
  - [داکر](config/docker.md)
